#pragma once

#include <algorithm>
#include <random>
#include <vector>

namespace Datasets {
    template <typename Type>
    class Dataset {
     public:
        Dataset() {}

        virtual std::size_t getLength() const = 0;
        virtual Type *getData() const = 0;

        virtual Dataset<Type> *shuffle() {
            Type *data = getData();

            // Source: https://stackoverflow.com/a/6127606/11964828
            for (std::size_t i = 0; i < getLength() - 1; i++)
            {
                std::size_t j = i + std::rand() / (RAND_MAX / (getLength() - i) + 1);
                Type t = data[j];
                data[j] = data[i];
                data[i] = t;
            }

            return this;
        }
    };
};
