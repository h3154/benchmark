#pragma once

#include <filesystem>
#include <vector>
#include <gio-unix-2.0/gio/gdesktopappinfo.h>
#include "datasets/dataset.hpp"

namespace Datasets {
    class Apps : public Dataset<char*> {
     protected:
        char **apps;
        std::vector<GDesktopAppInfo*> appVector;

        std::string locale, localeIndependentKey, localeDependentKey;

        inline const char *getName(GDesktopAppInfo *app) const;
        inline bool isDesktopFile(const std::filesystem::directory_entry &entry) const;
        void readDir(const std::string &dirname);

     public:
        Apps();
        virtual ~Apps();

        virtual std::size_t getLength() const;
        virtual char **getData() const;
    };
};
