#pragma once

#include <cstdlib>
#include <cstring>
#include "datasets/dataset.hpp"

namespace Datasets {
    class Alphabet : public Dataset<char*> {
     protected:
        char **letters;

     public:
        Alphabet();
        virtual ~Alphabet();

        virtual std::size_t getLength() const;
        virtual char **getData() const;
    };
};
