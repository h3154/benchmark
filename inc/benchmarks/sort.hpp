#pragma once
#include <vector>
#include "benchmarks/benchmark.hpp"

// What the fuck
// T* -> const T*
// Thanks a lot, vi#2943
template <typename T>
using pointer_to_const_pointer = typename std::add_pointer_t<std::add_const_t<std::remove_pointer_t<T>>>;

namespace Benchmarks {
    template <typename Type>
    class Sort : public Benchmark<Type>
    {
     protected:
        std::size_t length = 0;
        std::size_t size = 0;

        std::vector<Type> array;

        bool (*comparer)(pointer_to_const_pointer<Type>, pointer_to_const_pointer<Type>);

     public:
        Sort() = delete;
        Sort(
            const std::string &name,
            std::size_t length,
            bool (*comparer)(pointer_to_const_pointer<Type>, pointer_to_const_pointer<Type>)
        ) :
            Benchmark<Type>(name),
            length(length),
            size(sizeof(Type)),
            comparer(comparer)
        {
            array.resize(length);
        }

        virtual void setUp(Type *array) {
            for (std::size_t i = 0; i < length; i++) {
                this->array[i] = array[i];
            }
        }

        virtual void tearDown(Type *array) {
            for (std::size_t i = 0; i < length; i++) {
                array[i] = this->array[i];
            }
        }

        inline virtual void callback() {
            std::sort(array.begin(), array.end(), comparer);
        }
    };
};
