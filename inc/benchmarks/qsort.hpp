#pragma once
#include <cstdlib> // std::qsort, and __compar_fn_t?
#include "benchmarks/benchmark.hpp"

namespace Benchmarks {
    template <typename Type>
    class Qsort : public Benchmark<Type>
    {
     protected:
        std::size_t length = 0;
        std::size_t size = 0;

        Type *array;

        __compar_fn_t comparer;

     public:
        Qsort() = delete;
        Qsort(
            const std::string &name,
            std::size_t length,
            __compar_fn_t comparer
        ) :
            Benchmark<Type>(name),
            length(length),
            size(sizeof(Type)),
            comparer(comparer)
        {
            // ...
        }

        virtual void setUp(Type *array) {
            // The pointer is the same, so we're sorting the same array
            this->array = array;
        }

        virtual void tearDown(Type *array) {
            // The pointer is the same, so we just null down ours
            this->array = NULL;
        }

        inline virtual void callback() {
            std::qsort((void*)array, length, size, comparer);
        }
    };
};
