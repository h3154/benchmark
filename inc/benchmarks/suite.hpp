#pragma once

#include <array>
#include <iostream>
#include <fmt/core.h>
#include "benchmarks/benchmark.hpp"
#include "datasets/dataset.hpp"

namespace Benchmarks {
    template <typename Type, std::size_t count>
    class Suite {
     protected:
        Datasets::Dataset<Type> *dataset;
        std::array<Benchmarks::Benchmark<Type>*, count> benchmarks;
        std::size_t ran = 0;

     public:
        Suite() = delete;

        Suite(Datasets::Dataset<Type> *dataset, std::array<Benchmarks::Benchmark<Type>*, count> benchmarks) :
            dataset(dataset),
            benchmarks(benchmarks)
        {
            // ...
        }

        virtual ~Suite() {
            // ...
        }

        void run(std::size_t iterations) {
            ran += iterations;
            Type *array = (Type*)malloc(dataset->getLength() * sizeof(Type));

            for (std::size_t i = 0; i < iterations; i++) {
                Type *data = dataset->shuffle()->getData();
                for (auto benchmark : benchmarks) {
                    std::memcpy(array, data, dataset->getLength() * sizeof(Type));
                    benchmark->measureWith(array);
                }
            }

            free(array);
        }

        template <typename T, std::size_t C>
        friend std::ostream &operator<<(std::ostream &out, const Suite<T, C> &suite);
    };

    template <typename Type, std::size_t count>
    std::ostream &operator<<(std::ostream &out, const Suite<Type, count> &suite) {
        std::size_t longestName = 0, width = 3, precision = 3; // min, avg, and max are all 3 letters, hence the min 3
        for (const auto benchmark : suite.benchmarks) {
            longestName = std::max(longestName, benchmark->name().length());
            width = std::max<std::size_t>(width, fmt::format("{:Ld}", benchmark->max()).length());
        }

        out << fmt::format(
            "The dataset contained \e[01;33m{}\e[0m values\n",
            suite.dataset->getLength()
        );
        out << fmt::format(
            "The benchmark suite has ran \e[01;33m{:L}\e[0m times\n",
            suite.ran
        );
        out << fmt::format(
            "\e[01;33m{}\e[0m\n",
            "All the times are in µs!"
        );
        out << fmt::format(
            "\e[01;30m{:>{}s}: {:>{}s} / {:>{}s} / {:>{}s}\e[0m\n",
            "Algorithm", longestName,
            "min", width,
            "avg", width + precision + 1, // +1 for mantissa
            "max", width
        );

        for (const auto benchmark : suite.benchmarks) {
            out << fmt::format(
                "\e[01;32m{:>{}s}\e[0m: {:>{}s} / {:>{}s} / {:>{}s}\n",
                benchmark->name(), longestName,
                fmt::format("{:>{}Ld}", benchmark->min(), width), width,
                fmt::format("{:>{}.{}Lf}", benchmark->avg(), width + precision, precision), width + precision + 1, // +1 for mantissa
                fmt::format("{:>{}Ld}", benchmark->max(), width), width
            );
        }

        return out;
    }
};
