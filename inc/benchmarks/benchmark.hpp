#pragma once
#include <algorithm> // std::min, std::max
#include <cmath> // INFINITY
#include <sstream> // std::ostringstream
#include <sys/time.h> // gettimeofday, timeval

namespace Benchmarks {
    template <typename Type>
    class Benchmark
    {
     protected:
        unsigned int _min = -1;
        unsigned int _max = 0;
        unsigned int sum = 0;
        unsigned int count = 0;

        std::string _name;

        virtual void setUp(Type *arg) = 0;
        virtual void tearDown(Type *arg) = 0;

     public:
        Benchmark() = delete;
        Benchmark(const std::string& name) : _name(name) {
            // ...
        }

        std::string name() const {
            return _name;
        }

        unsigned int min() const {
            return _min;
        }

        double avg() const {
            return (double)sum / count;
        }

        unsigned int max() const {
            return _max;
        }

        inline virtual void callback() = 0;

        virtual void measureWith(Type *arg) {
            timeval start, end;
            setUp(arg);
            // We measure only the callback's time!
            gettimeofday(&start, NULL);
            callback();
            gettimeofday(&end, NULL);
            tearDown(arg);

            unsigned int delta = ((end.tv_sec - start.tv_sec) * 1000000) + (end.tv_usec - start.tv_usec);
            _min = std::min(_min, delta);
            _max = std::max(_max, delta);
            sum += delta;
            count++;
        }
    };
};
