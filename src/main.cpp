#include <iostream>
#include <locale>
#include "benchmarks/qsort.hpp"
#include "benchmarks/sort.hpp"
#include "benchmarks/suite.hpp"
#include "datasets/apps.hpp"

int main() {
    std::locale::global(std::locale("hu_HU.UTF-8"));
    Datasets::Apps apps;

    Datasets::Apps dataset;

    std::array<Benchmarks::Benchmark<char*>*, 8> benchmarks = {
        new Benchmarks::Qsort<char*>(
            "std::qsort(strcmp)",
            dataset.getLength(),
            [](const void *a, const void *b) {
                return std::strcmp(*(const char**)a, *(const char**)b);
            }
        ),
        new Benchmarks::Qsort<char*>(
            "std::qsort(g_strcmp0)",
            dataset.getLength(),
            [](const void *a, const void *b) {
                return g_strcmp0(*(const char**)a, *(const char**)b);
            }
        ),
        new Benchmarks::Qsort<char*>(
            "std::qsort(strcoll)",
            dataset.getLength(),
            [](const void *a, const void *b) {
                return std::strcoll(*(const char**)a, *(const char**)b);
            }
        ),
        new Benchmarks::Qsort<char*>(
            "std::qsort(g_utf8_collate)",
            dataset.getLength(),
            [](const void *a, const void *b) {
                return g_utf8_collate(*(const char**)a, *(const char**)b);
            }
        ),
        new Benchmarks::Sort<char*>(
            "std::sort(strcmp)",
            dataset.getLength(),
            [](const char *a, const char *b) {
                return std::strcmp(a, b) < 0;
            }
        ),
        new Benchmarks::Sort<char*>(
            "std::sort(g_strcmp0)",
            dataset.getLength(),
            [](const char *a, const char *b) {
                return g_strcmp0(a, b) < 0;
            }
        ),
        new Benchmarks::Sort<char*>(
            "std::sort(strcoll)",
            dataset.getLength(),
            [](const char *a, const char *b) {
                return std::strcoll(a, b) < 0;
            }
        ),
        new Benchmarks::Sort<char*>(
            "std::sort(g_utf8_collate)",
            dataset.getLength(),
            [](const char *a, const char *b) {
                return g_utf8_collate(a, b) < 0;
            }
        ),
    };

    Benchmarks::Suite suite(&dataset, benchmarks);

    suite.run(100000);
    std::cout << suite;

    for (auto benchmark : benchmarks) {
        delete benchmark;
    }

    return 0;
}
