#include "datasets/apps.hpp"

#include <cstdlib>
#include <cstring>
#include <iostream>

namespace Datasets {
    Apps::Apps() {
        // Set the locale and the keys
        locale = std::locale("").name().substr(0, 2);
        localeIndependentKey = "Name";
        localeDependentKey = localeIndependentKey + "[" + locale + "]";

        // Read the desktop files
        readDir("/usr/share/applications/");
        readDir(std::string(std::getenv("HOME")) + "/.local/share/applications");

        // Fill in the array from the vector
        apps = (char**)malloc(getLength() * sizeof(char*));
        for (std::size_t i = 0; i < getLength(); i++) {
            auto name = getName(appVector[i]);
            apps[i] = (char*)malloc((std::strlen(name) + 1) * sizeof(char)); // +1 for null terminator
            std::memcpy(apps[i], name, std::strlen(name) + 1); // +1 ffor null terminator
        }
    }

    Apps::~Apps() {
        for (std::size_t i = 0; i < getLength(); i++) {
            free(apps[i]);
        }
        free(apps);
    }

    bool Apps::isDesktopFile(const std::filesystem::directory_entry &entry) const {
        return entry.path().string().ends_with(".desktop");
    }

    const char *Apps::getName(GDesktopAppInfo *app) const {
        return g_desktop_app_info_get_string(app, localeDependentKey.c_str())
            ?: g_desktop_app_info_get_string(app, localeIndependentKey.c_str());
    }

    void Apps::readDir(const std::string &filename) {
        const std::filesystem::path dir(filename);
        std::size_t desktopCount = 0;
        // Count the number of .desktop files
        for (auto const& entry : std::filesystem::directory_iterator(dir)) {
            if (isDesktopFile(entry)) {
                desktopCount++;
            }
        }

        // MIGHT reserve a bit more than necessary, but we can live with that
        appVector.reserve(appVector.size() + desktopCount);

        // Read in the actual desktop files into a pre-allocated vector
        for (auto const& entry : std::filesystem::directory_iterator(dir)) {
            if (!isDesktopFile(entry)) {
                continue;
            }

            auto app = g_desktop_app_info_new_from_filename(entry.path().c_str());

            // wireshark.desktop has been returning null for some reason, so skip nulls
            // Also filter out apps with no name (like wine-extension-lgf)
            if (!app || !std::strlen(getName(app))) {
                continue;
            }

            appVector.push_back(app);
        }
    }

    char **Apps::getData() const {
        return apps;
    }

    std::size_t Apps::getLength() const {
        return appVector.size();
    }
};
