#include "datasets/alphabet.hpp"

namespace Datasets {
    Alphabet::Alphabet() {
        letters = (char**)std::malloc(sizeof(char*) * getLength());
        for (std::size_t i = 0; i < getLength(); i++) {
            letters[i] = (char*)std::calloc(sizeof(char), 2);
            letters[i][0] = ('a' + i);
        }
    }

    Alphabet::~Alphabet() {
        for (std::size_t i = 0; i < getLength(); i++) {
            free(letters[i]);
        }
        free(letters);
    }

    char **Alphabet::getData() const {
        return letters;
    }

    std::size_t Alphabet::getLength() const {
        return 26;
    }
};
